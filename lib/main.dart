import 'dart:ui';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

const appTitle = "Readington.X";
const cPrimary = Colors.deepPurple;
const cPrimaryLight = Color(0xFFF1E6FF);

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      theme: ThemeData(
        primarySwatch: cPrimary,
      ),
      home: const WelcomeScreen(title: appTitle),
      routes: <String, WidgetBuilder>{
        "/login": (BuildContext context) => const LoginScreen(title: "Login"),
        "/signup": (BuildContext context) => const SignUpScreen(title: "Sign Up"),
        "/profile": (BuildContext context) => const ProfileScreen(title: 'Profile'),
        "/homework": (BuildContext context) => const HomeScreen(title: 'Homework'),
        "/quizzes": (BuildContext context) => const HomeScreen(title: 'Quizzes'),
        "/gradebook": (BuildContext context) => const HomeScreen(title: 'Gradebook'),
        "/rank": (BuildContext context) => const HomeScreen(title: 'Ranking'),
        "/meetings": (BuildContext context) => const HomeScreen(title: 'Meetings'),
        "/forum": (BuildContext context) => const HomeScreen(title: 'Forum'),
      },
    );
  }
}

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({Key? key, required this.title}) : super(key: key);

  final String title;

  set height(double height) {}

  @override
  Widget build(BuildContext context) {
    BoxHeightStyle.includeLineSpacingTop;
    return (Expanded(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      const Text(
        'Welcome to $appTitle',
        style: TextStyle(
          fontWeight: FontWeight.bold,
          color: cPrimary,
        ),
        textAlign: TextAlign.center,
      ),
      Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        AppButton(
            text: "LOGIN",
            press: () {
              Navigator.pushNamed(context, "/login");
            }),
        AppButton(
            text: "SIGN UP",
            press: () {
              Navigator.pushNamed(context, "/signup");
            }),
      ]),
    ])));
  }
}

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key, required Comparable<String> title});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          leading: BackButton(onPressed: () {
            Navigator.pop(context);
          }),
          backgroundColor: cPrimary,
          centerTitle: true,
          title: const Text(appTitle),
        ),
        body: const LoginForm(),
      ),
    );
  }
}

class LoginForm extends StatefulWidget {
  const LoginForm({super.key});

  @override
  LoginFormState createState() {
    return LoginFormState();
  }
}

class LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          InputField(
            hintText: 'User Name',
            onChanged: (String value) {},
          ),
          PasswordField(
            // The validator receives the text that the user has entered.
            onChanged: (String value) {},
            hintText: '',
          ),
          Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 180.0,
            ),
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return const HomeScreen(
                    title: 'Dashboard',
                  );
                }));
              },
              style: ElevatedButton.styleFrom(
                primary: cPrimary,
              ),
              child: const Text('LOGIN'),
            ),
          ),
          const Text("Don't have an account? Create an account, click here"),
        ],
      ),
    );
  }
}

class SignUpScreen extends StatelessWidget {
  const SignUpScreen({super.key, required Comparable<String> title});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          leading: BackButton(onPressed: () {
            Navigator.pop(context);
          }),
          backgroundColor: cPrimary,
          centerTitle: true,
          title: const Text(appTitle),
        ),
        body: const SignUpForm(),
      ),
    );
  }
}

class SignUpForm extends StatefulWidget {
  const SignUpForm({super.key});

  @override
  SignUpFormState createState() {
    return SignUpFormState();
  }
}

class SignUpFormState extends State<SignUpForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
          InputField(
            hintText: "First Name",
            onChanged: (value) {},
          ),
          InputField(
            hintText: "Last Name",
            onChanged: (value) {},
          ),
          InputField(
            hintText: "Date of Birth",
            onChanged: (value) {},
          ),
          InputField(
            hintText: "Your Email",
            onChanged: (value) {},
          ),
          PasswordField(
            onChanged: (value) {},
            hintText: "Password",
          ),
          AppButton(
              text: "CREATE AN ACCOUNT",
              press: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return const LoginScreen(
                    title: "Login",
                  );
                }));
              }),
        ]));
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(appTitle),
        backgroundColor: cPrimary,
      ),
      body: const Text('Welcome to your Dashbard!'),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Log Out',
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return const LoginScreen(
              title: 'Dashboard',
            );
          }));
        },
        child: const Icon(Icons.logout),
        backgroundColor: cPrimary,
      ),
      drawer: const AppDrawer(),
    );
  }
}

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key, required this.title});
  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(title),
        backgroundColor: cPrimary,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const UserAvatar(),
          InputField(
            hintText: "First Name",
            onChanged: (value) {},
          ),
          InputField(
            hintText: "Last Name",
            onChanged: (value) {},
          ),
          InputField(
            hintText: "Date of Birth",
            onChanged: (value) {},
          ),
          InputField(
            hintText: "Your Email",
            onChanged: (value) {},
          ),
          PasswordField(
            onChanged: (value) {},
            hintText: "Password",
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
          tooltip: 'Back',
          onPressed: () {
            Navigator.pop(context);
          },
          backgroundColor: cPrimary,
          child: const Icon(Icons.arrow_back_rounded)),
      drawer: const AppDrawer(),
    );
  }
}



//AppButton //StringInputField //PasswordPasField
class AppButton extends StatelessWidget {
  final String text;
  final Function() press;
  final Color color, textColor;
  final double? height;
  const AppButton({
    Key? key,
    required this.text,
    required this.press,
    this.color = cPrimary,
    this.textColor = cPrimaryLight,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      height: height ?? size.height * 0.055,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(29),
        child: newElevatedButton(),
      ),
    );
  }

  Widget newElevatedButton() {
    return ElevatedButton(
      onPressed: press,
      style: ElevatedButton.styleFrom(
        primary: cPrimary,
        padding: const EdgeInsets.symmetric(
          horizontal: 40,
        ),
        textStyle: const TextStyle(
          color: cPrimaryLight,
          fontSize: 14,
          fontWeight: FontWeight.w500,
        ),
      ),
      child: Text(
        text,
        style: const TextStyle(
          color: cPrimaryLight,
        ),
      ),
    );
  }
}

class InputField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final ValueChanged<String> onChanged;
  const InputField({
    Key? key,
    required this.hintText,
    this.icon = Icons.person,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextFormField(
        onChanged: onChanged,
        cursorColor: cPrimary,
        decoration: InputDecoration(
          icon: Icon(
            icon,
            color: cPrimary,
          ),
          hintText: hintText,
          border: InputBorder.none,
        ),
      ),
    );
  }
}

class TextFieldContainer extends StatelessWidget {
  final Widget child;
  const TextFieldContainer({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: size.width * 0.8,
      decoration: BoxDecoration(
        color: cPrimaryLight,
        borderRadius: BorderRadius.circular(29),
      ),
      child: child,
    );
  }
}

class PasswordField extends StatelessWidget {
  final ValueChanged<String> onChanged;
  const PasswordField({
    Key? key,
    required this.onChanged,
    required Comparable<String> hintText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        obscureText: true,
        onChanged: onChanged,
        cursorColor: cPrimaryLight,
        decoration: const InputDecoration(
          hintText: "Password",
          icon: Icon(
            Icons.lock,
            color: cPrimary,
          ),
          suffixIcon: Icon(
            Icons.visibility,
            color: cPrimary,
          ),
          border: InputBorder.none,
        ),
      ),
    );
  }
}

class AppDrawer extends StatelessWidget {
  const AppDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          const DrawerHeader(
            decoration: BoxDecoration(
              color: cPrimary,
            ),
            child: UserAvatar(),
          ),
          ListTile(
            title: const Text(
              'Profile',
            ),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const ProfileScreen(
                  title: "Profile",
                );
              }));
            },
          ),
          ListTile(
            title: const Text('Homework'),
            onTap: () {
              Navigator.pushNamed(context, "/homework");
            },
          ),
          ListTile(
            title: const Text('Quizzes'),
            onTap: () {
              Navigator.pushNamed(context, "/quizzes");
            },
          ),
          ListTile(
            title: const Text('Gradebook'),
            onTap: () {
              Navigator.pushNamed(context, "/gradebook");
            },
          ),
          ListTile(
            title: const Text('Ranking'),
            onTap: () {
              Navigator.pushNamed(context, "/rank");
            },
          ),
          ListTile(
            title: const Text('Meetings'),
            onTap: () {
              Navigator.pushNamed(context, "/meetings");
            },
          ),
          ListTile(
            title: const Text('Forums'),
            onTap: () {
              Navigator.pushNamed(context, "/forums");
            },
          ),
        ],
      ),
    );
  }
}

class UserAvatar extends StatelessWidget {
  const UserAvatar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return const CircleAvatar(
        backgroundImage: AssetImage('assets/images/RM.png'),
          radius: 75,
          backgroundColor: cPrimaryLight,
        );
  }
}